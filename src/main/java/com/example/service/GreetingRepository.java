package com.example.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.entity.Greeting;


@RepositoryRestResource
public interface GreetingRepository extends JpaRepository<Greeting, Long> {
    
}
