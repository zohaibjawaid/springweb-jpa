package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Greeting {
    
    public Greeting() {
        
    }
    public Greeting(String message) {
        super();
        this.message = message;
    }
    @Id
    @GeneratedValue
    private long id;
    private String message;
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}